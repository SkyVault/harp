#ifndef HARP_LEXER_H
#define HARP_LEXER_H

#include "harp.h"

#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#define HARP_TOKEN_TYPES(X)\
    X(HARP_TOK_EOF)\
    X(HARP_TOK_ATOM)\
    X(HARP_TOK_NUMBER)\
    X(HARP_TOK_OPEN_PAREN)\
    X(HARP_TOK_CLOSE_PAREN)\
    X(HARP_TOK_OPEN_BRACKET)\
    X(HARP_TOK_CLOSE_BRACKET)\
    X(HARP_TOK_OPEN_BRACE)\
    X(HARP_TOK_CLOSE_BRACE)\
    X(HARP_TOK_QUOTE)

enum HarpTokTypes { HARP_TOKEN_TYPES(ENUM_E) };
static const char* HarpTokTypesS[] = { HARP_TOKEN_TYPES(ENUM_S) };

typedef struct {
    int type;
    const char *start, *end;
} HarpTok;

#define TokEOF (HarpTok){.type = HARP_TOK_EOF, .start = NULL, .end = NULL}

typedef struct {
    const char *it;
    const char *end;
} HarpLex;

HarpLex createLexer();

bool atEof(HarpLex* self);
void skipWS(HarpLex* self);

void loadLexer(HarpLex* self, const char* it);
void loadLexerBuffer(HarpLex* self, char* it, size_t len);

HarpLex createAndLoadLexer(const char* it);
HarpLex createAndLoadLexerBuffer(char* it, size_t len);

HarpTok nextToken(HarpLex* self);

#endif //HARP_LEXER_H
