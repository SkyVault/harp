#ifndef HARP_OBJECT_H
#define HARP_OBJECT_H

#include <stdlib.h>

#include "harp.h"

#define EACH_HARP_OBJ_TYPES(X) \
    X(HARP_NUMBER) \
    X(HARP_ATOM) \
    X(HARP_STRING) \
    X(HARP_BOOLEAN) \
    X(HARP_LIST)

enum HarpObjTypes { EACH_HARP_OBJ_TYPES(ENUM_E) };
static char* HarpObjTypesS[] = { EACH_HARP_OBJ_TYPES(ENUM_S) };

struct HarpObj;

typedef union {
    double number;
    int boolean;
    struct { char* buff; size_t len; } string;
    struct { char* buff; size_t len; } atom;
    struct { struct HarpObj* hd;
             struct HarpObj* tl; } list;
} HarpVal;

typedef struct {
    HarpVal val;
} HarpObj;

#endif//HARP_OBJECT_H
