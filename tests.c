#include "tests.h"
#include "lexer.h"

#ifndef CIMPLE_TESTS
#define CIMPLE_TESTS \
    CTEST(lexer_whitespace_test)\
    CTEST(lexer_number_test)\
    CTEST(lexer_single_char_test)\
    CTEST(lexer_atom_test)
#endif

#include "cimple/cimple.h"

bool lexer_whitespace_test(void) {
    HarpLex lex = createAndLoadLexer(" \n \t     X ");
    skipWS(&lex);

    if (atEof(&lex)) {
        cimp_log("At EOF");
        return false;
    }

    cimp_log("Ended at '%c'", *(lex.it));

    return *(lex.it) == 'X';
}

bool lexer_number_test(void) {
    double match[] = {-3.14159, 32, 4.0, .0, -.123};
    HarpLex lex = createAndLoadLexer(" -3.14159 32 4.0 .0 -.123");

    int i = 0;
    while (!atEof(&lex)) {
        HarpTok tok = nextToken(&lex);

        char buff[512];
        sprintf(buff, "%.*s", (int)(tok.end - tok.start), tok.start);

        if (i > sizeof(match)/sizeof(double)) {
            cimp_log("Failed to match each item");
            return false;
        }

        cimp_log("'%s' == %f", buff, match[i]);
        if (match[i] != strtod(buff, NULL)){ return false; }
        i+=1;
    }

    return true;
}

bool lexer_single_char_test(void) {
    int match[] = {
        HARP_TOK_OPEN_PAREN,
        HARP_TOK_OPEN_BRACKET,
        HARP_TOK_OPEN_BRACE,
        HARP_TOK_CLOSE_BRACE,
        HARP_TOK_CLOSE_BRACKET,
        HARP_TOK_CLOSE_PAREN,
    };

    HarpLex lex = createAndLoadLexer("([{}])");

    int i = 0;
    while (!atEof(&lex)) {
        HarpTok tok = nextToken(&lex);
        cimp_log("%s", HarpTokTypesS[tok.type]);
        if (tok.type != match[i++])
            return false;
    }
    return true;
}

bool lexer_atom_test(void) {
    const char* matches[] = {"hello", "wo23ld", "*mything*"};

    HarpLex lex = createAndLoadLexer("hello wo23ld *mything*");

    int i = 0;
    while (!atEof(&lex)) {
        HarpTok tok = nextToken(&lex);
        char buff[128];
        sprintf(buff, "%.*s", (int)(tok.end - tok.start), tok.start);
        cimp_log("%s == %s", buff, matches[i]);
        if (strcmp(buff, matches[i++]) != 0) {
            return false;
        }
    }

    return true;
}

void harpRunTests() {
    cimpleRunTests();
}
