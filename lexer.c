#include "lexer.h"

#define CH(l) ((l)->it < (l)->end ? (*((l)->it)) : '\0')

void loadLexer(HarpLex* self, const char* it) {
    self->it = it;
    self->end = it + strlen(it);
}

void loadLexerBuffer(HarpLex* self, char* it, size_t len) {
    self->it = it;
    self->end = it + len;
}

HarpLex createAndLoadLexer(const char* it) {
    HarpLex res;
    loadLexer(&res, it);
    return res;
}

HarpLex createAndLoadLexerBuffer(char* it, size_t size) {
    HarpLex res;
    loadLexerBuffer(&res, it, size);
    return res;
}

bool atEof(HarpLex* self) {
    return self->it == self->end;
}

void skipWS(HarpLex* self) {
    while (self->it != self->end && isspace(CH(self))) {
        self->it += 1;
    }
}

HarpTok nextToken(HarpLex* self) {
    skipWS(self);
    if (atEof(self)) return TokEOF;

    // Handle numbers
    {
        bool is_neg = false;
        bool is_dec = false;
        const char* start = self->it;

        if (CH(self) == '-') {
            is_neg = true;
            self->it += 1;
        }

        if (CH(self) == '.') {
            is_dec = true;
            self->it += 1;
        }

        if (isdigit(CH(self))) {
            if (!is_neg && !is_dec)
                start = self->it;

            while (!atEof(self)) {
                const char ch = CH(self);
                if (!isdigit(ch)) {
                    if (ch == '.') {
                        if (is_dec) { break; }
                        else is_dec = true;
                    } else { break; }
                }

                self->it += 1;
            }

            return (HarpTok) {.type=HARP_TOK_NUMBER, .start = start, .end = self->it };
        }
    }

    switch (CH(self)) {
        case '(': return (HarpTok) {.type=HARP_TOK_OPEN_PAREN, .start = self->it++ };
        case ')': return (HarpTok) {.type=HARP_TOK_CLOSE_PAREN, .start = self->it++ };
        case '[': return (HarpTok) {.type=HARP_TOK_OPEN_BRACKET, .start = self->it++ };
        case ']': return (HarpTok) {.type=HARP_TOK_CLOSE_BRACKET, .start = self->it++ };
        case '{': return (HarpTok) {.type=HARP_TOK_OPEN_BRACE, .start = self->it++ };
        case '}': return (HarpTok) {.type=HARP_TOK_CLOSE_BRACE, .start = self->it++ };
        case '\'': return (HarpTok) {.type=HARP_TOK_QUOTE, .start = self->it++ };
    }

    const char* start = self->it;
    while (!atEof(self) && !isspace(CH(self))){
        self->it++;
    }

    return (HarpTok) {.type = HARP_TOK_ATOM, .start = start, .end = self->it};
}

#undef CH
