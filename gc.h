#ifndef HARP_GC_H
#define HARP_GC_H

#include <stdlib.h>

#define HEAP_BLOCK_SIZE (2048)

typedef struct {
    unsigned char* heap;
} HarpGC;

typedef struct {
    int mark;
} HarpGCRef;

/* Steps

Q. How do we know if memory is no longer reachable
    1. no longer any active references to it

    we could use a tree

    ref {
        child_refs {
            child_refs {
            }
            next {
            }
        }
        next
    }

*/

static HarpGC* gc = NULL;

void harpGCInit();
void* harpGCAlloc(size_t size);
void* harpGCClean();

#endif//HARP_GC_H
